package hero;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Panel;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

public class GameMapPanel extends Panel {
	Map map;
	
	GameMapPanel(Map m) {
		map = m;
		GameKeyListener gkl = new GameKeyListener(this);
		GameMouseListener gml = new GameMouseListener(this);
		addKeyListener(gkl);
		addMouseListener(gml);
		this.setBackground(Color.green);
		}
	
		
		public void paint(Graphics g) {
			File f = new File ("asdf.png"); 
			
			int sizexInPixels = map.getSizex() * 30;
			int sizeyInPixels = map.getSizey() * 30;
			
			
			for(int i = 0; i <= map.getSizex(); i++) {
			    g.drawLine( i * 30, 0,0 + i* 30, sizeyInPixels);

			}
			for (int i = 0; i<= map.getSizey(); i ++) {
				g.drawLine(0, i * 30, sizexInPixels, i * 30);
			}
			try {
				BufferedImage bi = ImageIO.read(f);
				g.drawImage(bi, map.hero.posx, map.hero.posy, 30, 30, null);
			} catch (IOException e) {
				e.printStackTrace();
			}

		  }
		
	}
	

