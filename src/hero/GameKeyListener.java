package hero;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
public class GameKeyListener implements KeyListener {
	GameMapPanel panel;
	
	GameKeyListener(GameMapPanel p) {
		panel = p;
		
	}
	@Override
	public void keyTyped(KeyEvent e) {
		
	}
	@Override
	public void keyPressed(KeyEvent e) {
		char c = e.getKeyChar();
	    switch(c) {
	    	case 'w': panel.map.hero.setPosy(panel.map.hero.getPosy()-30); break;
	    	case 'a': panel.map.hero.setPosx(panel.map.hero.getPosx()-30); break;
	    	case 's': panel.map.hero.setPosy(panel.map.hero.getPosy()+30); break;
	    	case 'd': panel.map.hero.setPosx(panel.map.hero.getPosx()+30); break;
	    }
		panel.repaint();
	}

	@Override
	public void keyReleased(KeyEvent e) {
		// TODO Auto-generated method stub
		
		
	}

}
