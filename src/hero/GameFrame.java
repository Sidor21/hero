package hero;
import java.awt.Button;
import java.awt.Color;
import java.awt.Frame;
import java.awt.Graphics;
import java.awt.Label;
import java.awt.TextArea;
import java.awt.TextField;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

public class GameFrame  extends Frame {
	GameMapPanel mapPanel;
	public Label label;
	public Button button;
	TextField textField;
	TextArea textArea;
	
	GameFrame(Map m) {
		GameWindowListener gwl = new GameWindowListener();
		addWindowListener(gwl);

		mapPanel = new GameMapPanel(m);
		mapPanel.setBounds(30, 30, 181, 181);
		this.add(mapPanel);
		
		button = new Button();
		button.setBounds(240, 30, 100, 30);
		button.setLabel("Cukni na men");
		this.add(button);
		
		GameButtonActionListener al = new GameButtonActionListener(this);
		button.addActionListener(al);
		
	    label = new Label();
		label.setBounds(240,60, 300,30);
		label.setText("Name");
		this.add(label);
		
		textField = new TextField();
		textField.setBounds(m.sizex*30 + 60,150, 300, 30);
		textField.setText("write your name");
		this.add(textField);
		
		textArea = new TextArea();
		textArea.setBounds(m.sizex*30 + 60,210, 300, 90);
		textArea.setText("text");
		this.add(textArea);
	}
	

		
	}
	


